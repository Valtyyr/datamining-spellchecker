/*
 * Connecter.cpp
 *
 *  Created on: 25 juin 2014
 *      Author: valentin
 */

#include "connecter.h"

Connecter::Connecter() {}

Connecter::~Connecter() {}


void
Connecter::readFile(const char* iPathToWords, const char* iPathToBin) {
	Indexer lIndexer;
	std::ifstream lIfStream;
	lIfStream.open(iPathToWords);
	std::string lLine;
	if (lIfStream.is_open()) {
	    while (!lIfStream.eof()) {
	      getline(lIfStream, lLine);
	      if (lLine != "") {
	    	  lIndexer.addWord(tool::splitLine(lLine));
	      }
	    }
	} else {
		std::cout << "FAIL : File not found" << std::endl;
	}
	lIndexer.finish(iPathToBin);
}

void
Connecter::processConnection(const char* iPathToWord, const char* iPathToBin) {
	readFile(iPathToWord, iPathToBin);
}
