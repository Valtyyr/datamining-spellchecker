/*
 * Indexer.cpp
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#include "indexer.h"

Indexer::Indexer() {
    mTrie = new Trie();
}

Indexer::~Indexer() {}

void
Indexer::addWord(tyr::vector_string_type iContentLine) {
	unsigned int lFrequence = tool::stringToInteger(iContentLine[1]);
	mTrie->addWord(iContentLine[0], lFrequence);
}

Trie*
Indexer::getTrie() {
	return mTrie;
}

void
Indexer::finish(const char* iPathToBin) {
	mTrie->merge(mTrie->getRoot());
	srz::writeTrie(mTrie, iPathToBin);
}
