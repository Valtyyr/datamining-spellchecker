/*
 * Connecter.h
 *
 *  Created on: 25 juin 2014
 *      Author: valentin
 */

#ifndef CONNECTER_H_
# define CONNECTER_H_

# include "../../commons/header/all.h"
# include "../../commons/tools/tools.h"
# include "indexer.h"

class Connecter {
public:
	Connecter();
	virtual ~Connecter();

public:
	/**
	 * \brief EntryPoint of the connection
	 * \param iPathToWords the absolute path to all words with their frequence
	 * \param iPathToBin the absolute path to the file for serialization
	 */
	void processConnection(const char* iPathToWords, const char* iPathToBin);

private:
	/**
	 * \brief Collect all words in the input PathToWord
	 * Give those words to the Indexer
	 * \param iPathToWords the absolute path to all words with their frequence
	 * \param iPathToBin the absolute path to the file for serialization
	 */
	void readFile(const char* iPathToWords, const char* iPathToBin);
};

#endif /* CONNECTER_H_ */
