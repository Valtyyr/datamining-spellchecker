/*
 * Indexer.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef INDEXER_H_
# define INDEXER_H_

# include "../../commons/header/all.h"
# include "../../commons/tools/serialization.h"
# include "../../commons/tools/tools.h"
# include "../../commons/trie/trie.h"

class Indexer {
public:
	Indexer();
	virtual ~Indexer();

public:
	/**
	 * \brief Indexer simply call the add method
	 * of its Trie
	 * \param iContentLine the vector that contents the word and the frequence
	 */
	void addWord(tyr::vector_string_type iContentLine);

	/**
	 * \brief Compression from Trie to Patricia Trie
	 * And serialization of this Patricia Trie to the output file.
	 * \param iPathToBin the absolute path to the file for serialization
	 */
	void finish(const char* iPathToBin);

	/**
	 * \brief Get the Patricia Trie
	 */
	Trie* getTrie();

private:
	/**
	 * \biref Trie-Patricia Trie data structure
	 */
	Trie* mTrie;
};

#endif /* INDEXER_H_ */
