#include "../builder/connecter.h"

/*
 * EntryPoint of the Compilation
 */
int
main(int argc, char** argv) {
	if (argc != 3) {
		std::cout << "FAIL : Not good argument number to compile" << std::endl;
		return -1;
	} else {
		Connecter lConnecter;
		lConnecter.processConnection(argv[1], argv[2]);
	}
	return 0;
}
