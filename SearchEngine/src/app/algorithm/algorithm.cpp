/*
 * Algorithm.cpp
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

# include "algorithm.h"

namespace algo {
	uint8_t
	longestCommonSubstring(const std::string& iFirstWord, const std::string& iSecondWord) {
		 if (iFirstWord.empty() || iSecondWord.empty()) {
			  return 0;
		 }

		 int *lCurrent = new int [iSecondWord.size()];
		 int *lPrevious = new int [iSecondWord.size()];
		 int *lSwap = nullptr;
		 uint8_t lMaxSubstr = 0;

		 for (uint8_t i = 0; i < iFirstWord.size(); ++i) {
			  for (uint8_t j = 0; j < iSecondWord.size(); ++j) {
				   if (iFirstWord[i] != iSecondWord[j]) {
						lCurrent[j] = 0;
				   }
				   else {
						if (i == 0 || j == 0) {
							 lCurrent[j] = 1;
						}
						else {
							 lCurrent[j] = 1 + lPrevious[j-1];
						}
						lMaxSubstr = tool::max<int>(lMaxSubstr, lCurrent[j]);
				   }
			  }
			  lSwap = lCurrent;
			  lCurrent = lPrevious;
			  lPrevious = lSwap;
		 }
		 delete [] lCurrent;
		 delete [] lPrevious;
		 return lMaxSubstr;
	}

	uint8_t
	distanceDamerauLevenshtein(unsigned char* iFirstWord,
							   unsigned char* iSecondWord,
							   uint8_t iFirstLenght,
							   uint8_t iSecondLenght) {
		uint8_t lCost;
		uint8_t lDistance[iFirstLenght + 1][iSecondLenght + 1];

		for (uint8_t i = 0; i <= iFirstLenght; i++) {
			lDistance[i][0] = i;
		}

		for (uint8_t j = 1; j <= iSecondLenght; j++) {
			lDistance[0][j] = j;
		}

		for (uint8_t i = 1; i <= iFirstLenght; i++) {
			for (uint8_t j = 1; j <= iSecondLenght; j++) {
				if (iFirstWord[i - 1] == iSecondWord[j - 1]) {
					lCost = 0;
				} else {
					lCost = 1;
				}
				lDistance[i][j] =
						tool::min<uint8_t>(lDistance[i - 1][j] + 1,
								   lDistance[i][j - 1] + 1,
								   lDistance[i - 1][j - 1] + lCost);
				if (i > 1
					&& j > 1
					&& iFirstWord[i - 1] == iSecondWord[j - 2]
					&& iFirstWord[i - 2] == iSecondWord[j - 1]) {
					lDistance[i][j] =
							tool::min<uint8_t>(lDistance[i][j],
									   lDistance[i - 2][j - 2] + lCost);
				}
			}
		}
		return lDistance[iFirstLenght][iSecondLenght];
	}

	uint8_t
	processDistance(const std::string& iFirstWord, const std::string& iSecondWord) {
		unsigned char* lFirstChar = (unsigned char*) iFirstWord.c_str();
		unsigned char* lSecondChar = (unsigned char*) iSecondWord.c_str();
		uint8_t lResult = distanceDamerauLevenshtein(lFirstChar,
													 lSecondChar,
													 iFirstWord.length(),
													 iSecondWord.length());
		return lResult;
	}
}
