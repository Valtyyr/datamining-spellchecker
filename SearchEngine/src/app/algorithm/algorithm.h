/*
 * Algorithm.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef ALGORITHM_H_
# define ALGORITHM_H_

# include "../../commons/header/all.h"
# include "../../commons/tools/tools.h"

namespace algo {
	/**
	 * \brief Computation of the longest common substring
	 * between two strings
	 * \param iFirstWord the first word
	 * \param iSecondWord the second word
	 */
	uint8_t longestCommonSubstring(const std::string& iFirstWord, const std::string& iSecondWord);

	/**
	 * \brief Easy way to call Damerau-Levenshtein distance
	 * between two strings
	 * \param iFirstWord the first word to compare
	 * \param iSecondWord the second word to compare
	 */
	uint8_t processDistance(const std::string& iFirstWord, const std::string& iSecondWord);

	/**
	 * \brief Computation of the Damerau-Levenshtein distance
	 * \param iFirstWord the first word to compare
	 * \param iSecondWord the second word to compare
	 * \param iFirstLenght the lenght of the first word
	 * \param iSecondLenght the lenght of the second word
	 */
	uint8_t distanceDamerauLevenshtein(unsigned char* iFirstWord,
									   unsigned char* iSecondWord,
									   uint8_t iFirstLenght,
									   uint8_t iSecondLenght);
}

#endif /* ALGORITHM_H_ */
