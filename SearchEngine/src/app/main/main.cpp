#include "../../app/searcher/searcher.h"
#include "../../commons/tools/tools.h"

/*
 * EntryPoint of the Application
 */
int
main(int argc, char** argv) {
	if (argc != 2) {
		std::cout << "FAIL : Not good argument number to search" << std::endl;
		return -1;
	} else {
		Searcher lSearcher(argv[1]);
		std::string lInputLine;
		while (!std::cin.eof() && !std::cin.fail() && !std::cin.bad()) {
			getline(std::cin, lInputLine);
			std::stringstream lCommand(lInputLine, std::ios::in);
			std::string lCallFunction;
			lCommand >> lCallFunction;
			if (lCallFunction == "approx") {
				unsigned short int lDistance;
				lCommand >> lDistance;
				if (lCommand.fail())
					continue;

				std::string lWord;
				lCommand >> lWord;
				if (lWord.size() == 0)
					continue;

				lDistance = lDistance <= 0 ? 0 : lDistance;
				lSearcher.processSearch(lWord, lDistance);
				lSearcher.sortAndExport();
				lSearcher.clearResult();
			} else if (lCallFunction == "quit") {
				break;
			}
		}
	}
	return 0;
}
