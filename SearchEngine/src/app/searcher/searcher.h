/*
 * Searcher.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef SEARCHER_H_
# define SEARCHER_H_

# include "../../commons/header/all.h"
# include "../../commons/trie/trie.h"
# include "../../commons/tools/serialization.h"

	/**
	 * \brief Original word specified by the input
	 */
	static std::string sRefWord;

	/**
	 * \brief Resulting map of the search
	 */
	static std::map<Node*, std::pair<std::string, uint8_t>> sResultMap;

class Searcher {
public:
	/**
	 * Constructor
	 * \brief Deserialize for create a Patricia Trie
	 * \param iPath the binary path
	 */
	Searcher(const char* iPath);

	/**
	 * Destructor
	 */
	virtual ~Searcher();

public:
	/**
	 * \brief EntryPoint for all researchs
	 * \param iRefWord the original word
	 * \param iMaxDistance the maximal distance to looking for
	 */
	void processSearch(const std::string& iRefWord, const uint8_t& iMaxDistance);

	/**
	 * \brief Sort the result of the search
	 * and export it in JSon format
	 */
	void sortAndExport();

	/**
	 * \brief Clear the result of the just ended search
	 */
	void clearResult();


private:
	/**
	 * \brief Strict search for distance 0
	 * Check if the word exist in the
	 * Patricia Trie
	 */
	void searchStrict();

	/**
	 * \brief Approximative Damerau-Levenshtein
	 * recursive search in the Patricia Trie
	 * \param iNode the current node to inspect
	 * \param iPreviousWord the previous inspected word
	 * \param iPreviousRow the upper row of the current Damerau-Levenshtein row
	 * \param iPrePreviousRow the upper upper row of the Damerau-Levenshtein row
	 */
	void searchRecursive(Node* iNode,
			std::string iPreviousWord,
			std::vector<uint8_t> iPreviousRow,
			std::vector<uint8_t> iPrePreviousRow);

	/**
	 * \brief Sort the vector
	 * \param iVector the vector to sort
	 */
	void sortVector(std::vector<std::pair<Node*, std::string>>& iVector);

private:
	/**
	 * \brief Threshold distance
	 * specified by the user
	 */
	uint8_t mMaxDistance;

	/**
	 * \brief Size of the original word
	 * specified by the user
	 */
	uint8_t mRefWordSize;

	/**
	 * \brief Patricia Trie that contains
	 * all words from the dictionary
	 */
	Trie* mTrie;
};

#endif /* SEARCHER_H_ */
