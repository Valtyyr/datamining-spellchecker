/*
 * Searcher.cpp
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

# include "searcher.h"


Searcher::Searcher(const char* iPath) {
	mTrie = new Trie();
	srz::readTrie(mTrie, iPath);
	mRefWordSize = 0;
	mMaxDistance = 0;
}

Searcher::~Searcher() {}

struct CompareNode {
	inline bool operator()(std::pair<Node*, std::string> iFirstPair, std::pair<Node*, std::string> iSecondPair) {
		if (iFirstPair.first->getFrequence() > iSecondPair.first->getFrequence()) {
			return true;
		} else if (iFirstPair.first->getFrequence() == iSecondPair.first->getFrequence()) {
			bool lLexicoCompare = std::lexicographical_compare(iFirstPair.second.begin(),
															   iFirstPair.second.end(),
															   iSecondPair.second.begin(),
															   iSecondPair.second.end());
			if (lLexicoCompare) {
				return true;
			}
		}
		return false;
	}
};

void
Searcher::searchStrict() {
	Node* lCurrentNode = mTrie->getRoot();
	std::string lFinalWord = "";
    while (lCurrentNode != nullptr) {
        for (uint8_t i = 0; i < sRefWord.length(); ++i) {
        	Node* lTmpNode = nullptr;
        	std::string lCurrentWord = "";
        	for (uint8_t j = i; j < sRefWord.length(); ++j) {
				lCurrentWord += sRefWord[j];
        		lTmpNode = mTrie->findChild(lCurrentNode, lCurrentWord);
        		if (lTmpNode != nullptr) {
        			i = j;
        			break;
        		}
        	}
            if (lTmpNode == nullptr)
                break;
            lCurrentNode = lTmpNode;
            lFinalWord += lCurrentWord;
        }
        if (lFinalWord.compare(sRefWord) == 0) {
        	sResultMap.insert(std::pair<Node*, std::pair<std::string, uint8_t>>(lCurrentNode,
        					std::pair<std::string, uint8_t>(sRefWord, 0)));

            break;
        }
        else {
            break;
        }
    }
}

void
Searcher::searchRecursive(Node* iNode,
		std::string iPreviousWord,
		std::vector<uint8_t> iPreviousRow,
		std::vector<uint8_t> iPrePreviousRow) {
	uint8_t lColumns = mRefWordSize + 1;
	std::vector<uint8_t> lCurrentRow;
	lCurrentRow.reserve(lColumns);
    std::string lNodeContent = iNode->getContent();
    std::string lCurrentWord(iPreviousWord);
	uint8_t lCurrentWordSize = lCurrentWord.size();

	if (mMaxDistance == 1
		&& iPreviousWord != ""
		&& lCurrentWordSize == 1
		&& lCurrentWord[0] != sRefWord[0]
		&& lNodeContent[0] != sRefWord[0]
        && mRefWordSize > 1
		&& lCurrentWord[0] != sRefWord[1]
		&& lNodeContent[0] != sRefWord[1]) {
		return;
	}

	uint8_t lTranspositionCost = 0;
	uint8_t lDeleteCost = 0;
	uint8_t lInsertCost = 0;
	uint8_t lReplaceCost = 0;
	uint8_t lValue = 0;
	bool lPreviousIsEmpty = true;
	char lLetter = ' ';

	for (uint8_t j = 0; j < lColumns; ++j) {
		if (iPreviousRow[j] != 0) {
			lPreviousIsEmpty = false;
			break;
		}
	}

    for (size_t i = 0; i < lNodeContent.size(); ++i) {
    	lCurrentRow.clear();
		lCurrentRow.push_back(iPreviousRow[0] + 1);
    	lLetter = lNodeContent[i];
    	lCurrentWord += lLetter;
    	lCurrentWordSize++;

		for (uint8_t lColumn = 1; lColumn < lColumns; ++lColumn) {
			lDeleteCost = iPreviousRow[lColumn] + 1;
			lInsertCost = lCurrentRow[lColumn - 1] + 1;

			if (sRefWord[lColumn - 1] != lLetter) {
				lReplaceCost = iPreviousRow[lColumn - 1] + 1;
			} else {
				lReplaceCost = iPreviousRow[lColumn - 1];
			}

			lValue = tool::min<uint8_t>(lInsertCost, lDeleteCost, lReplaceCost);

			if (lColumn > 1
				&& !lPreviousIsEmpty
				&& sRefWord[lColumn - 1] == lCurrentWord[lCurrentWordSize - 2]
				&& sRefWord[lColumn - 2] == lLetter) {
				if (sRefWord[lColumn - 1] != lLetter) {
					lTranspositionCost = iPrePreviousRow[lColumn - 2] + 1;
				} else {
					lTranspositionCost = iPrePreviousRow[lColumn - 2];
				}
				lCurrentRow.push_back(tool::min<uint8_t>(lValue, lTranspositionCost));
			} else {
				lCurrentRow.push_back(lValue);
			}
		}
		if (lNodeContent.size() != 1 && i < lNodeContent.size() - 1) {
			iPrePreviousRow = iPreviousRow;
			iPreviousRow = lCurrentRow;
		}
    }

    if (lCurrentWord.size() > mRefWordSize + mMaxDistance) {
    	return;
    }

    if (lCurrentRow[mRefWordSize] <= mMaxDistance
    	&& iNode->getFrequence() != 0
    	&& lCurrentWordSize >= mRefWordSize - mMaxDistance) {
    	sResultMap.insert(std::pair<Node*, std::pair<std::string, uint8_t>>(iNode,
    					std::pair<std::string, uint8_t>(lCurrentWord, lCurrentRow[mRefWordSize])));

    }

    uint8_t lMinDistance = 255;
    for (uint8_t i = 0; i < lColumns; ++i) {
    	lMinDistance = tool::min<uint8_t>(lCurrentRow[i], lMinDistance);
    }

    if (lMinDistance <= mMaxDistance) {
        for (Node* lNode : iNode->getChildren()) {
            searchRecursive(std::move(lNode), lCurrentWord, lCurrentRow, iPreviousRow);
        }
    }
}

void
Searcher::processSearch(const std::string& iRefWord, const uint8_t& iMaxDistance) {
	sRefWord = iRefWord;
	mRefWordSize = sRefWord.size();
	if (iMaxDistance == 0) {
		searchStrict();
	} else {
		mMaxDistance = iMaxDistance;
		std::vector<uint8_t> lCurrentRow;
		lCurrentRow.reserve(mRefWordSize + 1);
		for (uint8_t i = 0; i < sRefWord.size() + 1; ++i) {
			lCurrentRow.push_back(i);
		}

		std::vector<uint8_t> lPreviousRow;
		lPreviousRow.reserve(mRefWordSize + 1);
		for (uint8_t i = 0; i < sRefWord.size() + 1; ++i) {
			lPreviousRow.push_back(0);
		}

		Node* lRootNode = mTrie->getRoot();
		for (Node* lNode : lRootNode->getChildren()) {
			searchRecursive(lNode, "", lCurrentRow, lPreviousRow);
		}
	}
}

void
Searcher::sortVector(std::vector<std::pair<Node*, std::string>>& iVector) {
	std::sort(iVector.begin(), iVector.end(), CompareNode());
}

void
Searcher::sortAndExport() {
	bool lComma = false;
	unsigned int lPrievousSize = 0;
	std::cout << "[";
	if (!sResultMap.empty()) {
		std::vector<std::pair<Node*, std::string>> lVector;
		for (uint8_t lDistance = 0; lDistance <= mMaxDistance; lDistance++) {
			for (std::pair<Node*, std::pair<std::string, uint8_t>> lPair : sResultMap) {
				if (lPair.second.second == lDistance) {
					lVector.push_back(std::pair<Node*, std::string>(std::pair<Node*, std::string>(lPair.first,
																								  lPair.second.first)));
				}
			}
			sortVector(lVector);
			lComma = lPrievousSize != 0 && lVector.size() != 0 ? true : false;
			for (unsigned int lIterator = 0; lIterator < lVector.size(); lIterator++) {
				std::pair<Node*, std::string> lPair = lVector[lIterator];
				Node* lNode = lPair.first;
				if (lComma)
					std::cout << ",";
				std::cout << "{\"word\":\"" + lPair.second + "\",\"freq\":"
							+ tool::integerToString(lNode->getFrequence()) + ",\"distance\":"
							+ tool::integerToString(lDistance) + "}";
				lComma = lIterator < lVector.size() - 1 ? true : false;
			}
			lPrievousSize = tool::max<uint8_t>(lVector.size(), lPrievousSize);
			lVector.clear();
		}
	}
	std::cout << "]" << std::endl;
}

void
Searcher::clearResult() {
	sResultMap.clear();
}
