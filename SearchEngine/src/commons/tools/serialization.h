/*
 * Serialization.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef SERIALIZATION_H_
#define SERIALIZATION_H_

#include "../header/all.h"
#include "../trie/trie.h"


namespace srz {
	/**
	 * \brief Entry call for the Trie serialization
	 * create the file if doesn't exist
	 * \param iTrie the Trie to serialize
	 * \param iPath the absolute path of the serialize file
	 */
	void writeTrie(Trie* iTrie, const char* iPath);

	/**
	 * \brief Entry call for the Trie deserialization
	 * \param iTrie the Trie to deserialize
	 * \param iPath the absolute path of the serialize file
	 */
	void readTrie(Trie* iTrie, const char* iPath);

	/**
	 * \brief Recursive serialization of nodes
	 * into the destination stream
	 * \param iNode the node to serialize
	 * \param iOut the output stream
	 */
	void writeNode(Node* iNode, std::ostream& iOut);

	/**
	 * \brief Threaded function for deserialization
	 * \param iTrie the Trie to build
	 * \param iIn the input stream
	 */
	void readRoot(Trie* iTrie, std::ifstream& iIn);

	/**
	 * \brief Recursive deserialization of nodes
	 * from the source stream
	 * \param iIn the input stream
	 */
	Node* readNode(std::ifstream& iIn);
}
#endif /* SERIALIZATION_H_ */
