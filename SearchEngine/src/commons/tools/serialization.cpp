# include "serialization.h"

namespace srz {
	void
	writeNode(Node* iNode, std::ostream &iOut) {
		if (iNode) {
			iOut << iNode->getContent() << " ";
			iOut << iNode->getFrequence() << " ";
			iOut << iNode->getChildren().size() << " ";
			for (Node* lSon : iNode->getChildren()) {
				writeNode(lSon, iOut);
			}
		}
	}

	void
	writeTrie(Trie* iTrie, const char* iPath) {
		std::fstream lFileStream;
		lFileStream.open(iPath);
		if (!lFileStream.is_open()) {
			lFileStream.open(iPath, std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
		}
		std::ofstream lOutFilestream(iPath);
		writeNode(iTrie->getRoot(), lOutFilestream);
		lOutFilestream.close();
	}

	Node*
	readNode(std::ifstream& iIn) {
		std::string lContent;
		uint16_t lNbSon = 0;
		uint32_t lFrequence = 0;
		iIn >> lContent;
		iIn >> lFrequence;
		iIn >> lNbSon;
		Node* lNode = new Node(lContent, lFrequence, lNbSon);
		for (uint16_t i = 0; i < lNbSon;  ++i) {
			Node* lChild = readNode(iIn);
			lNode->addChild(lChild);
		}
		return lNode;
	}

	void
	readRoot(Trie* iTrie, std::ifstream& iIn) {
		uint8_t lFrequence;
		uint16_t lNbSon;
		iIn >> lFrequence;
		iIn >> lNbSon;
		Node* lRoot = new Node(lFrequence, lNbSon);
		for (uint16_t i = 0; i < lNbSon; i++) {
			Node* lChild = readNode(iIn);
			lRoot->addChild(lChild);
		}
		iTrie->setRoot(lRoot);
	}

	void
	readTrie(Trie* iTrie, const char* iPath) {
		std::ifstream lInputFilestream(iPath);
		readRoot(iTrie, lInputFilestream);
		lInputFilestream.close();
	}
}
