/*
 * Node.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef NODE_H_
# define NODE_H_

# include "../../commons/header/all.h"

class Node {
public:
    Node();
    /**
     * Constructor
     * \param iFrequence the frequence of the node
     * \param iNbSon Pre allocation for the children vector
     */
    Node(unsigned int iFrequence, unsigned int iNbSon);

    /**
     * Constructor
     * \param iContent the node content
     * \param iFrequence the frequence of the node
     * \param iNbSon Pre allocation for the children vector
     */
    Node(std::string iContent, unsigned int iFrequence, unsigned int iNbSon);

    /**
     * Destructor
     */
    virtual ~Node();

public:
    /**
     * \brief Get the frequence
     */
    unsigned int getFrequence();

    /**
     * \brief Set the frequence
     */
    void setFrequence(unsigned int iFrequence);

    /**
     * \brief Get children of the node
     */
    std::vector<Node*> getChildren();

    /**
     * \brief Set children
     */
    void setChildren(std::vector<Node*> iChildren);

    /**
     * \brief Add a new node to the children
     */
    void addChild(Node* iNode);

    /**
     * \brief Set the content of the node
     */
    void setContent(std::string iContent);

    /**
     * \brief Get the content of node
     */
    std::string getContent();

protected:

    /**
     * \briefThe frequency of the node
     */
    unsigned int mFrequence;

    /**
     * \brief The content of the node
     */
    std::string mContent;

    /**
     * \brief All children of the node
     */
    std::vector<Node*> mChildren;

};

# include "node.hxx"

#endif /* NODE_H_ */
