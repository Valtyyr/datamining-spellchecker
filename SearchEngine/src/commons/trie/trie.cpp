/*
 * Trie.cpp
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#include "trie.h"

Trie::Trie() {
    mRoot = new Node();
}

Trie::~Trie() {}

Node*
Trie::findChild(Node* iNode, std::string iContent) {
    for (Node* lNode : iNode->getChildren()) {
        if (lNode->getContent() == iContent) {
            return lNode;
        }
    }
    return nullptr;
}

Node*
Trie::merge(Node* iNode) {
	for (Node* lNode : iNode->getChildren()) {
		merge(lNode);
	}
	if (iNode->getChildren().size() == 1) {
		Node* lSon = iNode->getChildren()[0];
		unsigned int lFatherFrequence = iNode->getFrequence();
		unsigned int lSonFrequence = lSon->getFrequence();
		if (lFatherFrequence == 0) {
			iNode->setFrequence(lSonFrequence);
			iNode->setContent(iNode->getContent() + lSon->getContent());
			iNode->setChildren(std::vector<Node*>());
			for (Node* lNode : lSon->getChildren()) {
				iNode->addChild(lNode);
			}
		}
	}
	return iNode;
}

void
Trie::addWord(std::string iWord, unsigned int iFrequence) {
    Node* lCurrentNode = mRoot;
    for (unsigned i = 0; i < iWord.length(); i++) {
    	Node* lChildNode = findChild(lCurrentNode, tool::charToString(iWord[i]));
        if (lChildNode != nullptr) {
            lCurrentNode = lChildNode;
        }
        else {
            Node* lNewNode = new Node();
            lNewNode->setContent(tool::charToString(iWord[i]));
            lCurrentNode->addChild(lNewNode);
            lCurrentNode = lNewNode;
        }
        if (i == iWord.length() - 1) {
            lCurrentNode->setFrequence(iFrequence);
        }
    }
}
