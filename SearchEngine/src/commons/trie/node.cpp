/*
 * Node.cpp
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#include "node.h"

Node::Node() {
	 mContent = ' ';
	 mFrequence = 0;
}

Node::Node(unsigned int iFrequence, unsigned int iNbSon)
	: mFrequence (iFrequence) {
	mChildren.reserve(iNbSon);
}

Node::Node(std::string iContent, unsigned int iFrequence, unsigned int iNbSon)
	: mFrequence (iFrequence),
	  mContent (iContent) {
	mChildren.reserve(iNbSon);
}

Node::~Node() {}
