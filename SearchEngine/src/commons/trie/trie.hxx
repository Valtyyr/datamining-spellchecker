/*
 * Trie.hxx
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

inline Node*
Trie::getRoot() {
	return mRoot;
}

inline void
Trie::setRoot(Node* iRoot) {
	mRoot = iRoot;
}
