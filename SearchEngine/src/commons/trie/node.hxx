/*
 * Node.hxx
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

inline std::string
Node::getContent() {
	return mContent;
}

inline void
Node::setContent(std::string iContent) {
	mContent = iContent;
}

inline unsigned int
Node::getFrequence() {
	return mFrequence;
}

inline void
Node::setFrequence(unsigned int iFrequence) {
	mFrequence = iFrequence;
}

inline void
Node::addChild(Node* iNode) {
	mChildren.push_back(iNode);
}

inline std::vector<Node*>
Node::getChildren() {
	return mChildren;
}

inline void
Node::setChildren(std::vector<Node*> iChildren) {
	mChildren = iChildren;
}
