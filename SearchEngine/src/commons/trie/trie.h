/*
 * Trie.h
 *
 *  Created on: 26 juin 2014
 *      Author: valentin
 */

#ifndef TRIE_H_
#define TRIE_H_

#include "../../commons/header/all.h"
#include "../../commons/tools/tools.h"
#include "node.h"

class Trie {
public:
    Trie();
    ~Trie();

public:
    /**
     * \brief Add a new word into the Trie
     * \param iWord the word to add
     * \param iFrequence the frequence of the word
     */
    void addWord(std::string iWord, unsigned int iFrequence);

    /**
     * \brief Find the child of the node that contains the
     * argument string
     * \param iNode the node to inspect
     * \param iContent the content to looking for
     */
    Node* findChild(Node* iNode, std::string iContent);

    /**
     * \brief Compress the Trie into a Patricia Trie
     * \param iNode the node to merge
     */
    Node* merge(Node* iNode);

public:
    /**
     * \brief Get the root node of the
     * Patricia Trie
     */
    Node* getRoot();

    /**
     * \brief Set the root node of the
     * Patricia Trie
     * \param iRoot the node to set
     */
    void setRoot(Node* iRoot);

private:
    /**
     * \brief Root node of the Patricia Trie
     */
	Node* mRoot;
};

#include "trie.hxx"

#endif /* TRIE_H_ */
