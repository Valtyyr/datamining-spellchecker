# README #

### Authors ###

* Valentin 'Valtyr' Macheret
* Tommy Nacass

### Overview ###

Advance version of the ClassicSearch from the same authors in order to make a spellcheck tool based on Damerau-Levenshtein distance.

### How do I set up ? ###

* Langage : C++ 11
* Compilation : CMake (2.8+)
* Compilation command : 42sh$ ./configure ; make
* Doc : Doxygen
* Doc command : 42sh$ make doc

### Run guidlines ###

#### Compilation binary ####

TextMiningCompiler create a dictionnary in a Patricia Trie datastructure. It finally serializes this Patricia Trie in an output file.

* Usage : 42sh$ ./TextMiningCompiler [words.txt] [dict.bin]
* Where [words.txt] is the path to file that contains word-frequence (line by line)
* And [dict.bin] is the path to the dictionnary which will be create for the serialization.

#### Run binary ####

TextMiningApp load the Patricia Trie in RAM from the input serialize file et allow to process research of a word in command line based on Damerau-Levenshtein distance.

* Usage : 42sh$ ./TextMininApp [dict.bin]
* Where [dict.bin] is the path to the serialize dictionnary.

* Then : approx [dist] [word]
* Where [dist] is the threshold Damerau-Levenshtein distance for [word].
* and [word] is the reference word of the research.